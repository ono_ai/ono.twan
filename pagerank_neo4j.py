from py2neo import Graph
from igraph import Graph as IGraph
graph = Graph("http://127.0.0.1:7474/", password = "123")

#query data from neo4j and upload it to igraph

query = '''
MATCH (u1:User)-[r:INTERACTS]->(u2:User)
RETURN u1.name, u2.name, r.weight AS weight
'''

ig = IGraph.TupleList(graph.run(query), weights=True)


pg = ig.pagerank()
pgvs = []
for p in zip(ig.vs, pg):
    print(p)
    pgvs.append({"name": p[0]["name"], "pg": p[1]})

#write pagerank data to db

write_clusters_query = '''
UNWIND {nodes} AS n
MATCH (u:User) WHERE u.name = n.name
SET u.pagerank = n.pg
'''

graph.run(write_clusters_query, nodes=pgvs)
